# Storage with Database Configuration by ConfigMap and Secret

## Step

Create files and config them.

```shell
mkdir k8s-adv/storage-db-config
# THEN
touch k8s-adv/storage-db-config/pv.yml
touch k8s-adv/storage-db-config/pv-claim.yml
touch k8s-adv/storage-db-config/deployment-postgres.yml
```

Before create them, check pv, pvc first.\
```shell
kubectl get pv
# NAME  CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS  CLAIM  STORAGECLASS  REASON  AGE
kubectl get pvc
# NAME  STATUS  VOLUME  CAPACITY  ACCESS MODES  STORAGECLASS  AGE
```

Create PV. Then check PV again.

```shell
kubectl apply -f k8s-adv/storage-db-config/pv.yml
# persistentvolume/pv-postgres created
## THEN
kubectl get pv
# NAME         CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS     CLAIM  STORAGECLASS  REASON  AGE
# pv-postgres  1Gi       RWO           Retain          Available         manual                52s
```

Create PVC. Then check PVC again.

```shell
kubectl apply -f k8s-adv/storage-db-config/pv-claim.yml
# persistentvolumeclaim/pvc-postgres created
## THEN
kubectl get pvc # We will see PVC's STATUS is `Bound`
# NAME          STATUS  VOLUME       CAPACITY  ACCESS MODES  STORAGECLASS  AGE
# pvc-postgres  Bound   pv-postgres  1Gi       RWO           manual        61s

kubectl get pv # We will see PV is already claimed by `default/pvc-postgres` and STATUS is `Bound`
# NAME         CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS  CLAIM                 STORAGECLASS  REASON  AGE
# pv-postgres  1Gi       RWO           Retain          Bound   default/pvc-postgres  manual                6m40s
```

Create Deployment and check it.

```shell
kubectl apply -f k8s-adv/storage-db-config/deployment-postgres.yml
# deployment.apps/deployment-postgres created

## THEN
kubectl get deployments
# NAME                 READY  UP-TO-DATE  AVAILABLE  AGE
# deployment-postgres  1/1    1           1          1m

## THEN Check pods
kubectl get pods -o wide
# NAME                                  READY  STATUS   RESTARTS  AGE  IP         NODE            NOMINATED NODE  READINESS GATES
# deployment-postgres-67bb65c58b-xhdqg  1/1    Running  0         66m  10.1.0.91  docker-desktop  <none>          <none>
```

Access to the Postgres Container.

```shell
kubectl exec -it deployment-postgres-67bb65c58b-xhdqg -- bash

## In the Container, run Postgres
psql -U <POSTGRES_USER> -l # use `-l` to list database names
## For example
psql -U mypostgres -l
## THEN we will see `my_database` following we config it in the `deployment-postgres.yml` file
# Name         Owner   Encoding  Locale Provider  Collate     Ctype       ICU Locale  ICU Rules  Access privileges
# my_database  myuser  UTF8      libc             en_US.utf8  en_US.utf8
```

Access to `my_database` database.

```shell
psql -U <POSTGRES_USER> -d <POSTGRES_DB>
## For example
psql -U myuser -d my_database

## Then check all tables in `my_database` database
\dt # In `psql`
# Did not find any relations.
```

Create table in `my_database` in **psql**.

```sql
CREATE TABLE users (
  id        SERIAL PRIMARY KEY,
  username  VARCHAR(50)   UNIQUE NOT NULL,
  email     VARCHAR(255)  UNIQUE NOT NULL
);
-- CREATE TABLE
```

Then check tables again.

```shell
\dt
#         List of relations
#  Schema | Name  | Type  | Owner
# --------+-------+-------+--------
#  public | users | table | myuser
# (1 row)
```

Insert row to `users` table.

```sql
INSERT INTO users (username, email) VALUES
  ('user1', 'user1@example.com'),
  ('user2', 'user2@example.com');
-- INSERT 0 2

--- THEN Query `users` table
SELECT * FROM users;
--  id | username |       email
-- ----+----------+-------------------
--   1 | user1    | user1@example.com
--   2 | user2    | user2@example.com
-- (2 rows)
```

### Test that PersistentVolume that will not be delete Volume when the Pod is dead.

Get Pod and delete it.

```shell
kubectl get pods
# NAME                                  READY  STATUS   RESTARTS  AGE
# deployment-postgres-67bb65c58b-xhdqg  1/1    Running  0         87m

kubectl delete pod/deployment-postgres-67bb65c58b-xhdqg
# pod "deployment-postgres-67bb65c58b-xhdqg" deleted

# Check pods, then we will see the new pod is re-creating (bec of using `replicas: 1` in the deployment definition)
kubectl get pods
# NAME                                  READY  STATUS             RESTARTS  AGE
# deployment-postgres-67bb65c58b-sjss5  0/1    ContainerCreating  0         7s

## THEN get pods again, we will see the new pod is running
kubectl get pods
# NAME                                   READY   STATUS    RESTARTS   AGE
# deployment-postgres-67bb65c58b-sjss5   1/1     Running   0          42s
```

The Pod new pod is re-created.\
So, we also need to delete _deployment_

```shell
kubectl delete deployment deployment-postgres
# deployment "deployment-postgres" deleted

kubectl get deployments
# No resources found in default namespace.
```

Create deployment again.

```shell
kubectl apply -f k8s-adv/storage-db-config/deployment-postgres.yml
# deployment.apps/deployment-postgres created

kubectl get all
# NAME                                      READY  STATUS   RESTARTS  AGE
# pod/deployment-postgres-67bb65c58b-g6n4p  1/1    Running  0         2m47s

# NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
# service/kubernetes  ClusterIP  10.96.0.1   <none>       443/TCP  17h

# NAME                                 READY  UP-TO-DATE  AVAILABLE  AGE
# deployment.apps/deployment-postgres  1/1    1           1          2m47s

# NAME                                            DESIRED  CURRENT  READY  AGE
# replicaset.apps/deployment-postgres-67bb65c58b  1        1        1      2m47s
```

Access the Pod that is created by this Deployment.

```shell
kubectl exec -it deployment-postgres-67bb65c58b-g6n4p -- bash

## In the container, access `psql`
psql -U myuser -l
#     Name     | Owner  | Encoding | Locale Provider |  Collate   |   Ctype    |
# -------------+--------+----------+-----------------+------------+-------------
#  my_database | myuser | UTF8     | libc            | en_US.utf8 | en_US.utf8 |

## THEN, access to the `my_database` database
psql -U myuser -d my_database

## THEN, list tables in the `my_database` database
\dt
#         List of relations
#  Schema | Name  | Type  | Owner
# --------+-------+-------+--------
#  public | users | table | myuser
# (1 row)
```

Query `users` table in `my_database` database.

```sql
SELECT * FROM users; -- We will see the data is still persisted
--  id | username |       email
-- ----+----------+-------------------
--   1 | user1    | user1@example.com
--   2 | user2    | user2@example.com
-- (2 rows)
```

It's still not secure.\
So, we will use *ConfigMap* and *Secret* instead.

Create ConfigMap and Secret definition files. Then, config them.
```shell
touch k8s-adv/storage-db-config/configmap-postgres.yml
touch k8s-adv/storage-db-config/secret-postgres.yml
```

Delete Deployment to clear old pod first.\
Then, check all.
```shell
kubectl delete deployment deployment-postgres
# deployment.apps "deployment-postgres" deleted

## THEN
kubectl get all
# NAME                    READY  STATUS   RESTARTS  AGE
# pod/pod-hostpath-nginx  1/1    Running  0         24h

# NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
# service/kubernetes  ClusterIP  10.96.0.1   <none>       443/TCP  26h
```

Create ConfigMap, Secret, PV, PVC, and Deployment.
```shell
kubectl apply -f k8s-adv/storage-db-config
# configmap/configmap-postgres created
# deployment.apps/deployment-postgres created
# persistentvolumeclaim/pvc-postgres unchanged
# persistentvolume/pv-postgres unchanged
# secret/secret-postgres created
```

Check PSQL in the container that is created by the Deployment
```shell
kubectl get pods
# NAME                                  READY  STATUS   RESTARTS  AGE
# deployment-postgres-7bccd769c6-gc78j  1/1    Running  0         4m20s

kubectl exec -it deployment-postgres-7bccd769c6-gc78j -- bash

## THEN
psql -U myuser -l
#                             List of databases
#     Name     | Owner  | Encoding | Locale Provider |  Collate   |   Ctype
# -------------+--------+----------+-----------------+------------+-------------
#  my_database | myuser | UTF8     | libc            | en_US.utf8 | en_US.utf8

psql -U myuser -d my_database
```

And then, query `users` table
```sql
SELECT * FROM users;
--  id | username |       email
-- ----+----------+-------------------
--   1 | user1    | user1@example.com
--   2 | user2    | user2@example.com
-- (2 rows)
```
