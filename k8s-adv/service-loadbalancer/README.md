Generate project name `service1` in [Spring Initializr](https://start.spring.io/#!type=maven-project&language=java&platformVersion=3.1.4&packaging=jar&jvmVersion=17&groupId=com.example&artifactId=service1&name=service1&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.service1&dependencies=web)\
Update the `Artifact` and `Name` field to be `service1`

We will get the zip file named `service1.zip`.\
THEN extract file and open project by preference IDE.
```shell
idea <path_to_directory>/service1 # open by IntelliJ IDEA
# OR
code <path_to_directory>/service1 # open by vscode
```

In `src/main/java/com/example/service1/Service1Application.java` file\
THEN update code like this.
```java
package com.example.service1;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Service1Application {
  public static void main(String[] args) {
    SpringApplication.run(Service1Application.class, args);
  }

  @GetMapping("/service1")
  public String hello(@RequestParam(value = "name", defaultValue = "service1") String name) {
    return "Hello, " + name;
  }
}
```

Build Spring App as a Jar file
```shell
mvn package
# OR
./mvnw package
```
THEN the `service1-0.0.1-SNAPSHOT.jar` file will appear in the `/target` directory.

Create Dockerfile in the SpringApplication project\
```shell
touch Dockerfile
```

Then add instruction command in a Dockerfile
```dockerfile
FROM openjdk:18

WORKDIR /app

COPY ./target/service1-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

CMD ["java", "-jar", "service1-0.0.1-SNAPSHOT.jar"]
```

Come back to this project.\
Build image from Dockerfile.\
Run this command in the root directory of this project.
```shell
docker build -t <dockerhub_username>/java-hello-microservice-1 <root_directory_which_has_dockerfile>
# FOR EXAMPLE
docker build -t korn704/java-hello-microservice-1 ~/Desktop/service1
```

Push image to Dockerhub
```shell
docker login
docker push <dockerhub_username>/java-hello-microservice-1
```

Create deployment-definition and service-definition files in the same directory of this `README.md` file.\
THEN config them.
```shell
touch deployment-loadbalancer.yml service-loadbalancer.yml
```

Create Service and Deployment
```shell
kubectl apply -f k8s-adv/service-loadbalancer
# OR each
kubectl apply -f k8s-adv/service-loadbalancer/deployment-loadbalancer.yml
kubectl apply -f k8s-adv/service-loadbalancer/service-loadbalancer.yml
```

