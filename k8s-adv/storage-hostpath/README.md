# HostPath Storage Type
## Step

Create files and config them.
```shell
mkdir k8s-adv/storage-hostpath
# THEN
touch k8s-adv/storage-hostpath/pod-hostpath.yml
touch k8s-adv/storage-hostpath/pv.yml
touch k8s-adv/storage-hostpath/pv-claim.yml
```

Then create Pod, PersistentVolume, and PersistentVolumeClaim
```shell
kubectl apply -f k8s-adv/storage-hostpath
# pod/pod-hostpath-nginx created
# persistentvolumeclaim/my-pvc created
# persistentvolume/my-pv created
```

Wait until they finished creating.\
Then, check them all.\
IF PV isn't connected with the PVC yet, THEN it will be `Available`.\
IF PV is already connect with the PVC, THEN it will be `Bound`.
```shell
kubectl get all
# NAME                    READY  STATUS   RESTARTS  AGE
# pod/pod-hostpath-nginx  1/1    Running  0         92s
#
# NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
# service/kubernetes  ClusterIP  10.96.0.1   <none>       443/TCP  104m

kubectl get pv
# NAME   CAPACITY  ACCESS MODES  RECLAIM POLICY  STATUS  CLAIM            STORAGECLASS   REASON   AGE
# my-pv  25Gi      RWO           Recycle         Bound   default/my-pvc   manual                  2m48s

kubectl get pvc
# NAME    STATUS  VOLUME  CAPACITY  ACCESS MODES  STORAGECLASS  AGE
# my-pvc  Bound   my-pv   25Gi      RWO           manual        3m34s
```

Test to access PV in the container.\
We will see the `data` directory in the path `/usr/share` which we mount path with in `pv.yml` and `pv-claim.yml` files.
```shell
kubectl exec -it pod-hostpath-nginx -- bash

ls /usr/share # in the container
base-passwd      common-licenses  dict         fontconfig  info  libgcrypt20  maven-repo
bash-completion  data             doc          fonts       java  lintian      menu
```

In the container, try to create file in `/usr/share/data` directory.\
Then, list file in that directory.
```shell
touch /usr/share/data/test.txt
## THEN
ls /usr/share/data
# test.txt
```

### Test that PersistentVolume type HostPath will not be delete Volume.
Delete Pod. Then, create Pod, PV, and PVC again.\
```shell
kubectl delete pod pod-hostpath-nginx
# pod "pod-hostpath-nginx" deleted

```shell
kubectl apply -f k8s-adv/storage-hostpath
## We will see only Pod that is created, otherwise is just unchanged
# pod/pod-hostpath-nginx created
# persistentvolumeclaim/my-pvc unchanged
# persistentvolume/my-pv unchanged
```

Access to the container again.\
Then, list files in `/usr/share/data`.\
We will still see the `test.txt` file.
```shell
kubectl exec -it pod-hostpath-nginx -- bash
## THEN
ls /usr/share/data
# test.txt
```
