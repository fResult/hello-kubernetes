# EmptyDir Storage Type
## Step

Create file and config it
```shell
mkdir k8s-adv/storage-emptydir
# THEN
touch k8s-adv/storage-emptydir/pod-emptydir.yml
```

Create Pods and mount Volume by this command
```shell
kubectl apply -f k8s-adv/storage-emptydir/pod-emptydir.yml
# pod/pod-linux created

## THEN check pods
kubectl get all
# NAME           READY  STATUS   RESTARTS  AGE
# pod/pod-linux  2/2    Running  0         29s
#
# NAME                TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
# service/kubernetes  ClusterIP  10.96.0.1   <none>       443/TCP  9m24s
```

Test them!\
First, test CentOS
```shell
kubectl exec -it pod-linux -c centos-container -- bash
# -it - Interactive mode with TTY
# -c  - Container
ls # in the container... [root@pod-linux /]#
## We will see `shared-data` directory
# bin  etc   lib    lost+found  mnt  proc  run   shared-data  sys  usr
# dev  home  lib64  media       opt  root  sbin  srv          tmp  var
```

Second, open new terminal tab, then test Ubuntu
```shell
kubectl exec -it pod-linux -c ubuntu-container -- bash
ls # in the container... root@pod-linux:/#
## We will also see `shared-data` directory
# bin   dev  home  media  opt   root  sbin         srv  tmp  var
# boot  etc  lib   mnt    proc  run   shared-data  sys  usr
```

List file in both `shared-data` directories of Centos and Ubuntu
```shell
ls shared-data # In CentOS
# Nothing

ls shared-data # In Ubuntu
# Nothing
```

Create text file in Ubuntu, then list file in CentOS
```shell
touch shared-data/test.txt # in Ubuntu
## THEN
ls shared-data # in CentOS, we will see the `test.txt` file
# test.txt

## THEN open test file by `vi` in CentOS
vi shared-data/test.txt # in CentOS, and add `Korn`, then save&exit file

## See the text in Ubuntu
cat shared-data/test.txt # in Ubuntu, we will see `Korn`
# Korn
```

Delete Pod, then create pod again
```shell
kubectl get pods
# NAME       READY  STATUS   RESTARTS     AGE
# pod-linux  2/2    Running  2 (53s ago)  61m

kubectl delete pod pod-linux
# pod "pod-linux" deleted

kubectl apply -f k8s-adv/storage-emptydir/pod-emptydir.yml
# pod/pod-linux created
```

Check `shared-data` directory in both containers.\
We will not see `test.txt` file, bec of the Pod is dead.\
And EmptyDir storage type will be also deleted when the Pod is deleted.
```shell
kubectl exec -it pod-linux -c centos-container -- bash
## AND
kubectl exec -it pod-linux -c ubuntu-container -- bash

## THEN
ls shared-data
# Nothing to show :(
```
