# Step

Create New Namespace named `ingress-nginx`
```shell
kubectl create namespace ingress-nginx
```

Install Kubernetes Package Manager\
Using Helm.sh
```shell
brew install helm
# OR
brew install kubernetes-helm
```

Add Nginx to Helm Repository
```shell
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

Then, Update Helm Repository
```shell
helm repo update
```

Install Helm's path
```shell
helm install ingress-nginx ingress-nginx/ingress-nginx -n ingress-nginx
```

Check pods in Namespace `ingress-nginx`
```shell
kubectl get pods -n ingress-nginx
# NAME                                         READY  STATUS    RESTARTS   AGE
# nginx-ingress-ingress-nginx-controller-xxxx  1/1    Running   0          3m35s
```

We already create `java-hello-microservice1`. So, now we will create `java-hello-microservice2`.\
Generate project name `service2` in [Spring Initializr](https://start.spring.io/#!type=maven-project&language=java&platformVersion=3.1.4&packaging=jar&jvmVersion=17&groupId=com.example&artifactId=service2&name=service2&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.service2&dependencies=web)\
Update the `Artifact` and `Name` field to be `service2`

In `src/main/java/com/example/service2/Service2Application.java` file\
THEN update code like this.
```java
package com.example.service2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Service2Application {

	public static void main(String[] args) {
		SpringApplication.run(Service2Application.class, args);
	}

	@GetMapping("/service2")
	public String hello(@RequestParam(value = "name", defaultValue = "service2") String name) {
		return "Hello, " + name;
	}
}
```

Build Spring App as a Jar file
```shell
mvn package
# OR
./mvnw package
```

Create Dockerfile in the SpringApplication project\
```shell
touch Dockerfile
```

Then add instruction command in a Dockerfile
```dockerfile
# Use `docker build -t korn704/java-hello-microservice-2 .`

FROM openjdk:18

WORKDIR /app

COPY ./target/service2-0.0.1-SNAPSHOT.jar /app

EXPOSE 8080

CMD ["java", "-jar", "service1-0.0.1-SNAPSHOT.jar"]
```

Come back to this project.\
Build image from Dockerfile.\
Run this command in the root directory of this project.
```shell
docker build -t <dockerhub_username>/java-hello-microservice-2 <root_directory_which_has_dockerfile>
# FOR EXAMPLE
docker build -t korn704/java-hello-microservice-2 ~/Desktop/service2
```

Push image to Dockerhub
```shell
docker login
docker push <dockerhub_username>/java-hello-microservice-2
```

Create Ingress Resources\
THEN config them
```shell
touch deployment-ingress.yml services.yml ingress.yml
```

THEN run
```shell
kubectl apply -f k8s-adv/ingress
```